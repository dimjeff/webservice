﻿using StreamJsonRpc;
using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Final_JsonRpc_Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            refreshListFromDb();
        }

        async void refreshListFromDb()
        {
            try
            {
                using (var stream = new NamedPipeClientStream(".", "StreamJsonRpcSamplePipe", PipeDirection.InOut, PipeOptions.Asynchronous))
                {
                    await stream.ConnectAsync();
                    var jsonRpc = JsonRpc.Attach(stream);
                    string[] tlist = await jsonRpc.InvokeAsync<string[]>("GetAllTravels");
                    List<string> lst = tlist.OfType<string>().ToList();
                    TripList.ItemsSource = lst;
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Database error\n" + ex.Message, "Database error");

            }
        }

        private async void addTrip_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            if (name.Length < 2 || name.Length > 50)
            {
                MessageBox.Show("The name length must be 2-50 chars", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            string passport = tbPassport.Text;
            if (passport.Length != 8)
            {
                MessageBox.Show("The passport length must be 8 chars", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Regex rgx = new Regex(@"[A-Z]{2}[0-9]{6}");
            if (!rgx.IsMatch(passport))
            {
                MessageBox.Show("The Passport must be AB123456 format. ", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            double cost;
            if(!double.TryParse(tbCost.Text, out cost))
            {
                MessageBox.Show("The cost must be number. ", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (cost<0 || cost> 1000000)
            {
                MessageBox.Show("cost is between 0 and 1000000. ", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            try
            {
                using (var stream = new NamedPipeClientStream(".", "StreamJsonRpcSamplePipe", PipeDirection.InOut, PipeOptions.Asynchronous))
                {
                    await stream.ConnectAsync();
                    var jsonRpc = JsonRpc.Attach(stream);
                    await jsonRpc.InvokeAsync("AddTravel", name, passport, cost);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Database error\n" + ex.Message, "Database error");
                return;
            }
            refreshListFromDb();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_JsonRpc_Server
{
    class TravelPlannerService
    {
        SQLiteConnection conn;

        public TravelPlannerService()
        {
            conn = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            // Open the connection:
            try
            {
                conn.Open();
                SQLiteCommand sqlite_cmd;
                string Createsql = "CREATE TABLE if not exists Travels (Id INTEGER PRIMARY KEY, Name NVARCHAR(50), Passport NVARCHAR(50), Cost Double)";
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = Createsql;
                sqlite_cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("DB error: " + ex.Message);
            }
        }

        public void AddTravel(String name, String passport, double cost)
        {
            try
            {
                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = "INSERT INTO Travels (Name, Passport, Cost) VALUES(?, ?, ?); ";
                SQLiteParameter dName = new SQLiteParameter();
                SQLiteParameter dPassport = new SQLiteParameter();
                SQLiteParameter dCost = new SQLiteParameter();
                sqlite_cmd.Parameters.Add(dName);
                sqlite_cmd.Parameters.Add(dPassport);
                sqlite_cmd.Parameters.Add(dCost);
                dName.Value = name;
                dPassport.Value = passport;
                dCost.Value = cost;
                sqlite_cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Data add error: " + ex.Message);
            }
        }

        public String[] GetAllTravels()
        {
            SQLiteDataReader sqlite_datareader;
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Travels";
            List<string> allTraList = new List<string>();
            sqlite_datareader = sqlite_cmd.ExecuteReader();
            while (sqlite_datareader.Read())
            {
                //3: Jerry (AB123456) $56.45
                int id = sqlite_datareader.GetInt32(0);
                string myreader = id + ": " + sqlite_datareader.GetString(1) +" (" + sqlite_datareader.GetString(2) + ") $" + sqlite_datareader.GetDouble(3);
                allTraList.Add(myreader);
            }
            conn.Close();
            return allTraList.ToArray();
        }
    }
}

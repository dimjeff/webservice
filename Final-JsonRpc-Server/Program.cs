﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StreamJsonRpc;

namespace Final_JsonRpc_Server
{
    class Program
    {
        static void Main(string[] args)
        {
            //SQLiteConnection sqlite_conn;
            //sqlite_conn = CreateConnection();
            //CreateTable(sqlite_conn);
            //InsertData(sqlite_conn);
            //ReadData(sqlite_conn);

            /*            TravelPlannerService ts = new TravelPlannerService();
                        ts.AddTravel("Yee", "WE323232", 32.1);
                        ts.AddTravel("Lee", "PE323232", 42.1);

                        string[]  str = ts.GetAllTravels();
                        for(int i = 0; i < str.Length; i++)
                        {
                            Console.WriteLine(str[i]);
                        }
                        Console.ReadKey();*/

            MainAsync().GetAwaiter().GetResult();
        }
        static async Task MainAsync()
        {
            int clientId = 0;
            while (true)
            {
                Console.WriteLine("Waiting for client to make a connection...");
                var stream = new NamedPipeServerStream("StreamJsonRpcSamplePipe", PipeDirection.InOut, NamedPipeServerStream.MaxAllowedServerInstances, PipeTransmissionMode.Byte, PipeOptions.Asynchronous);
                await stream.WaitForConnectionAsync();
                Task nowait = ResponseToRpcRequestsAsync(stream, ++clientId);
            }
        }

        private static async Task ResponseToRpcRequestsAsync(NamedPipeServerStream stream, int clientId)
        {
            Console.WriteLine($"Connection request #{clientId} received. Spinning off an async Task to cater to requests.");
            var jsonRpc = JsonRpc.Attach(stream, new TravelPlannerService());
            Console.WriteLine($"JSON-RPC listener attached to #{clientId}. Waiting for requests...");
            await jsonRpc.Completion;
            Console.WriteLine($"Connection #{clientId} terminated.");
        }
    }
}

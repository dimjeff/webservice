﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StreamJsonRpc;

namespace Day03_StreamJsonRpc_Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void Add_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            using (var stream = new NamedPipeClientStream(".", "StreamJsonRpcSamplePipe", PipeDirection.InOut, PipeOptions.Asynchronous))
            {
                await stream.ConnectAsync();
                var jsonRpc = JsonRpc.Attach(stream);
                await jsonRpc.InvokeAsync("Add", name);
            }
        }

        private async void GetAll_Click(object sender, RoutedEventArgs e)
        {
            using (var stream = new NamedPipeClientStream(".", "StreamJsonRpcSamplePipe", PipeDirection.InOut, PipeOptions.Asynchronous))
            {
                await stream.ConnectAsync();
                var jsonRpc = JsonRpc.Attach(stream);
                string[] flist = await jsonRpc.InvokeAsync<string[]>("GetAll");
                foreach(string str in flist)
                {
                    tbFriendList.Text += tbFriendList.Text + str + "\r\n";
                }
            }
        }
    }
}

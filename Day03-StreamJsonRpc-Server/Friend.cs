﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Day03_StreamJsonRpc_Server
{
    public class Friend
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ApplicationDbContext : DbContext
    {
        //add-migration AddToDb
        //update-database
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=FriendsDB;Trusted_Connection=True;MultipleActiveResultSets=true");
        }
        public DbSet<Friend> Friends { get; set; }

    }
}

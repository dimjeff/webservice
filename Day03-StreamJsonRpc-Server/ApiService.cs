﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Day03_StreamJsonRpc_Server
{
    class ApiService
    {
        public void Add(string name)
        {
            if (name.Length == 0)
            {
                return;
            }
            ApplicationDbContext db = new ApplicationDbContext();
            Friend f = new Friend();
            f.Name = name;
            db.Friends.Add(f);
            db.SaveChanges();
            db.Dispose();
        }

        public string[] GetAll()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            int count =  db.Friends.Count();
            string[] fList = new string[count];
            for(int i = 0; i < count; i++)
            {
                fList[i] = db.Friends.ToArray()[i].Name;
            }
            db.Dispose();
            return fList;
        }
    }
}
